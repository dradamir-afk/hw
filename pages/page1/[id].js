import Link from "next/link"
import {useRouter} from "next/router"

export default function Absabs(){
    const router = useRouter()
    return (
        <div>
            <Link href={'/page1'}><a>Go to the page 1</a></Link>
        </div>
    )
}