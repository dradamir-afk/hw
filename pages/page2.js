import Link from 'next/link'

export default function Page2({data}){


    return(
        <div>
            <Link href={'/page1'}><a>Go to the Page 1</a></Link>
            <hr/>
            <Link href={'/page1/absabs'}><a>Go to the Page 1/absabs</a></Link>
            <p>{data.text}</p>
        </div>
    )
}

export async function getServerSideProps({query, req}) {
    const res = await fetch('http://localhost:3000/api/hello')
    const data = await res.json()
    return {props: {data}}
}