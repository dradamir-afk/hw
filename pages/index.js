import Head from 'next/head'
import styles from '../styles/Home.module.css'
import Link from 'next/link'

export default function Home() {
  return (
    <div>
      <Link href={'/page1'}><a>Go to the Page 1</a></Link>
    </div>
  )
}
